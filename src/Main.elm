module Main exposing (..)

import Browser
import Browser.Events as Event
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode
import Random
import Svg
import Svg.Attributes as Attr exposing (..)
import Time



-- MAIN


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view =
            \model ->
                { title = "Snake"
                , body = [ showHeader, showBody model, showFooter ]
                }
        }



-- MODEL


type alias Model =
    { isPlaying : Bool
    , snake : Snake
    , food : Food
    , score : Int
    }


type alias Snake =
    { position : ( Int, Int )
    , length : Int
    , body : List ( Int, Int )
    , direction : Direction
    }


type alias Food =
    ( Int, Int )



-- INIT


playingField =
    createPlayingField 1 1


createPlayingField : Int -> Int -> List ( Int, Int )
createPlayingField x y =
    if x > 100 then
        if y > 100 then
            []

        else
            createPlayingField 1 (y + 1)

    else
        ( x, y ) :: createPlayingField (x + 1) y


snakeStartBody =
    [ ( 50, 50 ), ( 49, 50 ), ( 48, 50 ), ( 47, 50 ) ]


startingFood =
    ( 13, 27 )


initModel : Model
initModel =
    { isPlaying = False
    , snake = startingSnake
    , food = startingFood
    , score = 0
    }


startingSnake : Snake
startingSnake =
    { position =
        case List.head snakeStartBody of
            Just x ->
                x

            Nothing ->
                ( 50, 50 )
    , length = List.length snakeStartBody
    , body = snakeStartBody
    , direction = Right
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( initModel
    , Cmd.none
    )



-- UPDATE


type Msg
    = MoveSnake Time.Posix
    | FoodGenerated Food
    | StartPlaying
    | Moved Direction


type Direction
    = Up
    | Down
    | Left
    | Right
    | Other


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        MoveSnake time ->
            let
                newSnake =
                    snakeMove model.snake

                isHit =
                    isSnakeHit newSnake.body

                hasEaten =
                    isFoodEaten newSnake model.food
            in
            if isHit then
                ( { model | isPlaying = False }, Cmd.none )

            else if hasEaten then
                ( { model | snake = newSnake, score = model.score + 1 }, createNewFood )

            else
                ( { model | snake = newSnake }, Cmd.none )

        FoodGenerated ( x, y ) ->
            if isFoodLocationOk model.snake.body ( x, y ) then
                ( { model
                    | food = ( x, y )
                    , snake =
                        { length = model.snake.length + 1
                        , direction = model.snake.direction
                        , body = model.snake.body
                        , position = model.snake.position
                        }
                  }
                , Cmd.none
                )

            else
                ( model, createNewFood )

        StartPlaying ->
            ( { model | isPlaying = True, snake = startingSnake, food = startingFood, score = 0 }, Cmd.none )

        Moved dir ->
            let
                newSnake =
                    adjustDirection model.snake dir
            in
            ( { model | snake = newSnake }, Cmd.none )


adjustDirection : Snake -> Direction -> Snake
adjustDirection snake dir =
    case snake.direction of
        Up ->
            { snake | direction = directionFixVertical Up dir }

        Down ->
            { snake | direction = directionFixVertical Down dir }

        Left ->
            { snake | direction = directionFixHorizontal Left dir }

        Right ->
            { snake | direction = directionFixHorizontal Right dir }

        _ ->
            snake


directionFixVertical : Direction -> Direction -> Direction
directionFixVertical original dir =
    case dir of
        Left ->
            Left

        Right ->
            Right

        _ ->
            original


directionFixHorizontal : Direction -> Direction -> Direction
directionFixHorizontal original dir =
    case dir of
        Up ->
            Up

        Down ->
            Down

        _ ->
            original


createNewFood =
    Random.generate FoodGenerated newFoodGenerator


newFoodGenerator : Random.Generator Food
newFoodGenerator =
    Random.pair (Random.int 1 99) (Random.int 1 99)


isFoodLocationOk : List ( Int, Int ) -> Food -> Bool
isFoodLocationOk list food =
    not (List.member food list)


isFoodEaten : Snake -> Food -> Bool
isFoodEaten snake food =
    snake.position == food


isSnakeHit : List ( Int, Int ) -> Bool
isSnakeHit list =
    case list of
        [] ->
            False

        x :: xs ->
            List.member x (List.reverse xs)


snakeMove : Snake -> Snake
snakeMove snake =
    let
        newPosition =
            snakeMoveHead snake.position snake.direction
    in
    { snake
        | body = List.take snake.length (newPosition :: snake.body)
        , position = newPosition
    }


snakeMoveHead : ( Int, Int ) -> Direction -> ( Int, Int )
snakeMoveHead ( x, y ) dir =
    case dir of
        Up ->
            if y - 1 < 0 then
                ( x, 100 )

            else
                ( x, y - 1 )

        Down ->
            if y + 1 > 100 then
                ( x, 0 )

            else
                ( x, y + 1 )

        Left ->
            if x - 1 < 0 then
                ( 100, y )

            else
                ( x - 1, y )

        Right ->
            if x + 1 > 100 then
                ( 0, y )

            else
                ( x + 1, y )

        _ ->
            ( x, y )



-- SUBSCRIPTIONS


moveTimer : Float
moveTimer =
    50.0


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.isPlaying then
        Sub.batch
            [ Time.every moveTimer MoveSnake
            , keyPressed
            ]

    else
        Sub.none


keyPressed : Sub Msg
keyPressed =
    Event.onKeyPress keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toDirection (Decode.field "key" Decode.string)


toDirection : String -> Msg
toDirection string =
    case string of
        "ArrowLeft" ->
            Moved Left

        "Left" ->
            Moved Left

        "ArrowRight" ->
            Moved Right

        "Right" ->
            Moved Right

        "ArrowUp" ->
            Moved Up

        "Up" ->
            Moved Up

        "ArrowDown" ->
            Moved Down

        "Down" ->
            Moved Down

        "w" ->
            Moved Up

        "a" ->
            Moved Left

        "s" ->
            Moved Down

        "d" ->
            Moved Right

        _ ->
            Moved Other



-- VIEW


rectWidth : Int
rectWidth =
    505


rectHeigth : Int
rectHeigth =
    rectWidth


showHeader : Html Msg
showHeader =
    header [ align "center" ]
        [ p [ align "center" ] [ text "Snake" ]
        ]


showFooter : Html Msg
showFooter =
    footer [ align "bottom" ]
        [ p [ align "center" ] [ text "By: haba22" ]
        ]


showBody : Model -> Html Msg
showBody model =
    div [ align "center" ]
        [ p [ align "center" ] [ text ("score: " ++ String.fromInt model.score) ]
        , div [ align "center" ]
            [ Svg.svg
                [ Attr.width (String.fromInt rectWidth)
                , Attr.height (String.fromInt rectHeigth)
                , Attr.viewBox ("0 0 " ++ String.fromInt rectWidth ++ " " ++ String.fromInt rectHeigth)
                ]
                (square :: drawFood model.food :: drawSnake model.snake.body)
            ]
        , button [ onClick StartPlaying ] [ text "Play" ]
        ]


square =
    Svg.rect
        [ x (String.fromInt 0)
        , y (String.fromInt 0)
        , Attr.width (String.fromInt rectWidth)
        , Attr.height (String.fromInt rectHeigth)
        , Attr.stroke "black"
        , Attr.fill "white"
        ]
        []


drawFood : ( Int, Int ) -> Svg.Svg msg
drawFood food =
    lineFunction food


drawSnake : List ( Int, Int ) -> List (Svg.Svg msg)
drawSnake body =
    List.map lineFunction body


lineFunction : ( Int, Int ) -> Svg.Svg msg
lineFunction ( x, y ) =
    Svg.rect
        [ Attr.x (String.fromInt (x * 5))
        , Attr.y (String.fromInt (y * 5))
        , Attr.width (String.fromInt 5)
        , Attr.height (String.fromInt 5)
        , Attr.stroke "black"
        , Attr.fill "black"
        ]
        []
